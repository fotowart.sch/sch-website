<?php
// Check for empty fields
if(empty($_POST['reason'])      ||
   empty($_POST['name'])      ||
   empty($_POST['phone'])     ||
   empty($_POST['email'])     ||
   empty($_POST['message'])   ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
   echo "No arguments Provided!";
   return false;
   }
   
$seniorReason = 'Fragen zur Verbindung';
$mietwartReason = 'Fragen zur Studentenwohnung';
$conseniorReason = 'Fragen zu Veranstaltungen';
$fmReason = 'Wie kann ich Mitglied werden?';

$reason = strip_tags(htmlspecialchars($_POST['reason']));
$name = strip_tags(htmlspecialchars($_POST['name']));
$email_address = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));
   
// Disect witch email address should be chosen

if ($reason == $seniorReason):
    $to = 'senior@kdstv-schwarzwald.de';
elseif ($reason == $mietwartReason):
	$to = 'nils.bieler@web.de';
    //$to = 'zimmer@kdstv-schwarzwald.de';
elseif ($reason == $conseniorReason):
    $to = 'consenior@kdstv-schwarzwald.de';
elseif ($reason == $fmReason):
    $to = 'fuxmajor@kdstv-schwarzwald.de';
else:
    //$to = 'info@kdstv-schwarzwald.de';
    $to = 'nils.bieler@googlemail.de';
endif;

$email_subject = "Website Contact Form:  $name";
$email_body = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nPhone: $phone\n\nMessage:\n$message";
$headers = "From: noreply@yourdomain.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$headers .= "Reply-To: $email_address";   
mail($to,$email_subject,$email_body,$headers);
return true;         
?>